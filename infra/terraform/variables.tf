variable "region" {
    description = "Please Enter region"
    type        = string
    default     = "us-east-1"
}

variable "instance_type" {
    description = "Enter instance_type"
    type        = string
    default     = "t2.micro"
}

variable "allow_ports" {
    description = "Listof Port to open for server"
    type        = list
    default     = ["80", "443", "22", "8080", "9090", "9100"]
}

variable "common_tags" {
    description = "Common Tag all resources"
    type        = map 
    default = {
      Name       = "ServerWebIP"
      Owner      = "Alexey"
      Project    = "Diplom"
      Enviroment = "dev"
    }
}

variable "site_rote53" {
    description = "site add A record "
    type        = string
    default     = "skillbox-diplom.ml"
}

variable "ssl_certificate_id" {
    description = "ssl sert"
    type = string
    default = "arn:aws:iam::180621436052:server-certificate/diplom_name"
}